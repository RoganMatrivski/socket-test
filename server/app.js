const http = require('http').createServer();
const io = require('socket.io')(http);

const PORT = process.env.PORT | 1234;

io.on('connection', (socket) => {
    console.log(`${socket.client.conn.remoteAddress} are connected to this server.`);

    socket.on('disconnect', () => {
        console.log(`${socket.client.conn.remoteAddress} disconnected.`);
    })

    socket.on('msg_broadcast', function(msg) {
        console.log(`${socket.client.conn.remoteAddress} : ${msg.question} = ${msg.answer}`);

        const newAnswer = randMsg(msg.answer);
        console.log(`Server : ${newAnswer.question} = ${newAnswer.answer}`);
        socket.emit('msg_broadcast', newAnswer)
    })
})

http.listen(PORT, () => {   
    console.log(`Listening on port ${PORT}`);
})

const rng = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const randOp = [
    (num) => {
        const randnum = rng(-100, 100);
        return {
            question: num + " + " + randnum,
            answer: num + randnum,
        };
    },
    (num) => {
        const randnum = rng(-100, 100);
        return {
            question: num + " - " + randnum,
            answer: num - randnum,
        };
    },
];

const randMsg = (number) => {
    return randOp[rng(0, randOp.length - 1)](number);
}