const PORT = process.env.PORT | 1234;

console.log(`connecting to http://server:${PORT}`)

const io = require('socket.io-client')(`http://server:${PORT}`);

const rng = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const randOp = [
    (num) => {
        const randnum = rng(-100, 100);
        return {
            question: num + " + " + randnum,
            answer: num + randnum,
        };
    },
    (num) => {
        const randnum = rng(-100, 100);
        return {
            question: num + " - " + randnum,
            answer: num - randnum,
        };
    },
];

const randMsg = (number) => {
    return randOp[rng(0, randOp.length - 1)](number);
}

io.on('connect', (socket) => {
    console.log(`Connected to server.`);
    io.emit('msg_broadcast', randMsg(0));

    io.on('msg_broadcast', (msg) => {
        console.log(`server : ${msg.question} = ${msg.answer}`);

        const newAnswer = randMsg(msg.answer);
        console.log(`Client : ${newAnswer.question} = ${newAnswer.answer}`);
        io.emit('msg_broadcast', newAnswer)
    })
})